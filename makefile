CMD = stow
FLAGS = --no-folding

BASE = vim zsh
EXTRA = git dircolors rofi xorg zathura todo ranger weechat gnupg kitty qutebrowser mutt vimiv i3 khal

.PHONY: all base extra init uninstall $(BASE) $(EXTRA)

all: base extra

base: $(BASE)

extra: init $(EXTRA)

init:
	git submodule update --recursive --init

update:
	git submodule update --recursive --remote

uninstall:
	$(CMD) $(FLAGS) -D $(BASE) $(EXTRA)

$(EXTRA) $(BASE)::
	$(CMD) $(FLAGS) $@

vim::
	git submodule update --init -- lib/vim-plug
	git submodule update --init -- lib/elixir-ls
	nvim --headless +PlugInstall +PlugClean! +qa

zsh::
	git submodule update --init -- lib/powerlevel9k
	git submodule update --init -- lib/zsh-autosuggestions
	git submodule update --init -- lib/zsh-completions
	git submodule update --init -- lib/zsh-syntax-highlighting
	git submodule update --init -- lib/z
