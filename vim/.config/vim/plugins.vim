call plug#begin('~/.config/vim/plugins')

" General
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-dispatch'
Plug 'xolox/vim-misc'
Plug 'editorconfig/editorconfig-vim'
Plug 'dbeniamine/vim-mail'
Plug 'mhinz/vim-startify'
Plug 'zef/vim-cycle'

" Navigation
Plug 'Shougo/defx.nvim'
Plug 'Shougo/denite.nvim'
Plug 'Shougo/neomru.vim'
Plug 'neoclide/denite-git'

" Textobj
Plug 'kana/vim-textobj-user'
Plug 'blackheaven/vim-textobj-function'

" colorscheme
Plug 'chriskempson/base16-vim'

" Snippets
" Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Tags
Plug 'majutsushi/tagbar'                      , { 'on': 'TagbarToggle' }

" Undo
Plug 'mbbill/undotree'

" Coloration
Plug 'luochen1990/rainbow'
Plug 'vim-scripts/AfterColors.vim'

" Completion
Plug 'neoclide/coc.nvim'                      , { 'do': 'yarn install --frozen-lockfile' }
Plug 'jsfaint/gen_tags.vim'

" Text navigation
Plug 'unblevable/quick-scope'

" Text manipulation
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'junegunn/vim-easy-align'

" Syntax checking
Plug 'w0rp/ale'

" Git
Plug 'tpope/vim-fugitive'
Plug 'jreybert/vimagit'
Plug 'rhysd/git-messenger.vim'

" UI
Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'
Plug 'Yggdroot/indentLine'
Plug 'ryanoasis/vim-devicons'

" Org
Plug 'vimwiki/vimwiki'
Plug 'tbabej/taskwiki'

" Test
Plug 'janko-m/vim-test'

" Ruby
Plug 'tpope/vim-bundler'                      , { 'for': 'ruby' }
Plug 'tpope/vim-rails'                        , { 'for': 'ruby' }
Plug 'tek/vim-textobj-ruby'                   , { 'for': 'ruby' }

" Javascript
Plug 'ncm2/ncm2-tern'                         , { 'for': 'js', 'do': 'npm install' }

" Css
Plug 'ap/vim-css-color'                       , { 'for': ['scss', 'sass', 'css'] }
Plug 'ncm2/ncm2-cssomni'                      , { 'for': ['scss', 'sass', 'css'] }

" Elm
Plug 'elmcast/elm-vim'                        , { 'for': 'elm' }

" Python
Plug 'ncm2/ncm2-jedi'                         , { 'for': 'python' }
Plug 'bps/vim-textobj-python'                 , { 'for': 'python' }

" Latex
Plug 'lervag/vimtex'                          , { 'for': 'latex' }

" Markdown
Plug 'suan/vim-instant-markdown'

" The rest
Plug 'sheerun/vim-polyglot'

call plug#end()
