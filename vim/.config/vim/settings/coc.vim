" " if hidden is not set, TextEdit might fail.
set hidden

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

let g:coc_global_extensions = [
\   "coc-git",
\   "coc-json",
\   "coc-html",
\   "coc-css",
\   "coc-vimtex",
\   "coc-tsserver",
\   "coc-snippets",
\   "coc-tag",
\   "coc-syntax",
\   "coc-vetur",
\   "coc-solargraph",
\   "coc-python",
\   "coc-yaml",
\]

" use <tab> for trigger completion and navigate to next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" snippets
imap <c-j> <plug>(coc-snippets-expand)

let g:coc_snippet_next = "<c-t>"
let g:coc_snippet_previous = "<c-s>"

" coc-git
" navigate chunks of current buffer
nmap gt <Plug>(coc-git-nextchunk)
nmap gs <Plug>(coc-git-prevchunk)
" Hunk-add and hunk-revert for chunk staging
nnoremap <silent> <leader>ga :CocCommand git.chunkStage<CR>
nnoremap <silent> <leader>gu :CocCommand git.chunkUndo<CR>
" show chunk diff at current position
nmap <leader>gi <Plug>(coc-git-chunkinfo)
" show commit ad current position
nmap <leader>gc <Plug>(coc-git-commit)
