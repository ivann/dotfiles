if has('nvim')
  set viminfo+=n$HOME/.cache/nvim/shada.vim
else
  set ttymouse=xterm2
  set viminfo+=n$HOME/.cache/vim/viminfo
end

if !exists("g:syntax_on")
  syntax enable
endif

filetype plugin indent on

let mapleader="\<space>"
let maplocalleader=","

set encoding=utf-8

set clipboard+=unnamedplus

" ui
set linespace=2
set guioptions=iacr
set ruler
set cursorline
" hybrid line number
set relativenumber
set number
set scrolloff=3
set showcmd
set title
set showtabline=2
set diffopt+=vertical
set inccommand=nosplit

" fold
set foldmethod=syntax
set foldnestmax=10
set nofoldenable
set foldlevel=2

" search
set infercase
set smartcase
set showmatch
set hlsearch
set incsearch
set gdefault

" dev
set autoindent
set expandtab
set shiftround
set tabstop=2
set shiftwidth=2
set softtabstop=2

" history
set history=1000

" completion
set wildmenu
set wildmode=list:longest,full

set backspace=2
set nostartofline

" highlight nbsd
highlight NbSp ctermbg=lightred guibg=lightred
match NbSp /\%xa0/

" Save backup file to $HOME/.cache/vim/backup
if isdirectory($HOME . '/.cache/vim/backup') == 0
  :silent !mkdir -p $HOME/.cache/vim/backup >/dev/null 2>&1
endif

set backup
set backupdir=$HOME/.cache/vim/backup,.

" Save swap file to $HOME/.cache/vim/swap
if isdirectory($HOME . '/.cache/vim/swap') == 0
  :silent !mkdir -p $HOME/.cache/vim/swap >/dev/null 2>&1
endif

set directory=$HOME/.cache/vim/swap,.

if has('persistent_undo')
  " Save undo file to $HOME/.cache/vim/undo
  if isdirectory($HOME . '/.cache/vim/undo') == 0
    :silent !mkdir -p $HOME/.cache/vim/undo >/dev/null 2>&1
  endif

  set undofile
  set undodir=$HOME/.cache/vim/undo,.
endif

" use w!! to save as root
cmap w!! w !sudo tee % >/dev/null

augroup fix_filetype
  autocmd!

"  autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g'\"" | endif
  autocmd BufNewFile,BufReadPost *.md set filetype=markdown
  autocmd BufNewFile,BufReadPost *.json.jbuilder set filetype=ruby
  autocmd BufNewFile,BufReadPost *.nse set filetype=lua
  autocmd BufNewFile,BufReadPost *.nasm set filetype=nasm
  autocmd BufNewFile,BufReadPost *.slime set filetype=slim
  autocmd BufNewFile,BufReadPost *.muttrc set filetype=muttrc
augroup END

augroup enable_spell
  autocmd!

  autocmd FileType gitcommit setlocal spell spellcapcheck=
  autocmd FileType rst,tex setlocal spell
augroup END

augroup color_fix
    autocmd!
    " autocmd ColorScheme nord highlight Identifier ctermfg=6
    " autocmd ColorScheme base16-* highlight ALEWarning ctermbg=8
    " autocmd ColorScheme base16-* highlight ALEError ctermbg=8
    autocmd ColorScheme base16-* highlight SpellBad ctermbg=18
    autocmd ColorScheme base16-* highlight SpellCap ctermbg=18
augroup END

let base16colorspace=256  " Access colors present in 256 colorspace
color base16-onedark

set list listchars=nbsp:¬,tab:\|\ ,extends:»,precedes:«

set wildignore+=*/tmp/*,*.so,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.jpeg,*.png,*.xpm,*.gif,*.pdf,*.bak,*.avi,*.mp3,*.mp4,*.ogg,*.wmv,*.wma,*.ogm,*.exe,*.dll,*.bmp,*.zip,*.odt,*.gz,*.bz2
