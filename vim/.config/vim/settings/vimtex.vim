let g:vimtex_mappings_disable = {
  \ 'n': ['cse', 'csc', 'cs$', 'csd', 'tsc', 'tse', 'tsd', 'tsD'],
  \ 'x': ['cse', 'csc', 'cs$', 'csd', 'tsc', 'tse', 'tsd', 'tsD'],
  \}

let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'
let g:vimtex_compiler_method = 'latexmk'
let g:vimtex_compiler_latexmk_engines = {
    \ '_' : '-lualatex',
    \}
