let g:ale_sign_error="\uf05e"
let g:ale_sign_warning="\uf071"

let g:ale_sign_column_always = 1

autocmd ColorScheme * highlight ALEErrorSign ctermfg=red ctermbg=18

let g:ale_fix_on_save = 1

let g:ale_linters = {
\   'javascript': ['standard'],
\   'elixir': [],
\}

let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['standard'],
\   'elixir': ['mix_format'],
\}

nmap <silent> <C-s> <Plug>(ale_previous_wrap)
nmap <silent> <C-t> <Plug>(ale_next_wrap)
