set omnifunc=phpcomplete#CompletePHP

hi! def link phpDocTags       phpDefine
hi! def link phpDocParam      phpType
hi! def link phpDocIdentifier phpIdentifier
