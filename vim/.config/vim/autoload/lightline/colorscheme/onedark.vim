" =============================================================================
" Filename: autoload/lightline/colorscheme/materia.vim
" Author: Lokesh Krishna
" License: MIT License
" Last Change: 2017/10/21 11:32:27.
" =============================================================================

" Common colors
let s:fg     = [ '#c8ccd4', 188 ]
let s:blue   = [ '#61afef', 75 ]
let s:green  = [ '#98c379', 114 ]
let s:purple = [ '#c678dd', 176 ]
let s:red    = [ '#e06c75', 168 ]
let s:yellow = [ '#e5c07b', 180 ]
let s:bg     = [ '#282c34', 18 ]
let s:gray1  = [ '#565c64', 59 ]
let s:gray2  = [ '#abb2bf', 145 ]

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:p.normal.left     = [ [ s:bg, s:blue, 'bold' ], [ s:fg, s:gray1 ] ]
let s:p.normal.middle   = [ [ s:bg, s:gray2 ] ]
let s:p.inactive.left   = [ [ s:gray1,  s:bg, 'bold' ], [ s:gray2, s:gray1 ] ]
let s:p.inactive.right  = [ [ s:gray2, s:gray1, 'bold' ], [ s:gray2, s:gray1, 'bold' ] ]
let s:p.insert.left     = [ [ s:bg, s:green, 'bold' ], [ s:fg, s:gray1 ] ]
let s:p.replace.left    = [ [ s:bg, s:red, 'bold' ], [ s:fg, s:gray1 ] ]
let s:p.visual.left     = [ [ s:bg, s:purple, 'bold' ], [ s:fg, s:gray1 ] ]
let s:p.normal.right   = [ [ s:fg, s:gray1, 'bold' ], [ s:fg, s:gray1, 'bold' ] ]
let s:p.normal.error   = [ [ s:red,   s:bg ] ]
let s:p.normal.warning = [ [ s:yellow, s:bg ] ]
let s:p.tabline.left   = [ [ s:gray2, s:gray1 ] ]
let s:p.tabline.tabsel = [ [ s:bg, s:gray2 ] ]
let s:p.tabline.middle = [ [ s:gray2, s:gray1 ] ]
let s:p.tabline.right  = [ [ s:bg, s:gray2, 'bold' ], [ s:bg, s:gray2, 'bold' ] ]

let g:lightline#colorscheme#onedark#palette = lightline#colorscheme#flatten(s:p)
