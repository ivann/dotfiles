
share_dir="/usr/share/fzf"

if [ -n "${commands[fzf-share]}" ]
then
    share_dir="$(fzf-share)"
fi

[[ $- == *i* ]] && source "$share_dir/completion.zsh" 2> /dev/null
source "$share_dir/key-bindings.zsh"


export FZF_DEFAULT_COMMAND="fd --type file --follow --color=always"
export FZF_DEFAULT_OPTS="--height=80% \
                         --ansi
                         --reverse \
                         --history-size=1000000000 \
                         --bind 'ctrl-t:down' \
                         --bind 'ctrl-s:up' \
                         --bind 'ctrl-p:toggle-preview' \
                         --bind 'ctrl-f:page-down' \
                         --bind 'ctrl-b:page-up' \
                         --bind 'ctrl-d:half-page-down' \
                         --bind 'ctrl-u:half-page-up' \
                         --bind 'ctrl-e:jump-accept' \
                        "

export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--preview 'mime=\$(file --mime {});
                                   (
                                     ([[ \$mime =~ directory ]] && tree -L 3 -C {}) ||
                                     ([[ \$mime =~ symlink ]] && readlink {}) ||
                                     ([[ \$mime =~ binary ]] && echo \"\") ||
                                     bat --plain --color always {}
                                   ) 2> /dev/null'"

export FZF_ALT_C_COMMAND="fd --type directory --follow --color=always"
export FZF_ALT_C_OPTS="--preview 'tree -L 3 -C {}'"

export FZF_COMPLETION_OPTS="$FZF_DEFAULT_OPTS"

_fzf_compgen_path() {
    fd --hidden --follow --exclude ".git" --color=always . "$1"
}

_fzf_compgen_dir() {
    fd --type directory --follow --hidden --exclude ".git" --color=always . "$1"
}
