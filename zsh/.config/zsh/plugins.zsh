ZPLUGDIR=$ZDOTDIR/plugins

### POWERLEVEL9K

zsh_nix_shell(){
    [ -n "$IN_NIX_SHELL" ] && echo "nix"
}

POWERLEVEL9K_MODE='nerdfont-complete'
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"
POWERLEVEL9K_SHORTEN_DIR_LENGTH=4
POWERLEVEL9K_STATUS_VERBOSE=false
POWERLEVEL9K_CUSTOM_NIX_SHELL="zsh_nix_shell"
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir dir_writable custom_nix_shell virtualenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status background_jobs)

source $ZPLUGDIR/powerlevel9k/powerlevel9k.zsh-theme


### zsh-completions

fpath+=($ZPLUGDIR/zsh-completions/src)


### zsh-autosuggestions

source $ZPLUGDIR/zsh-autosuggestions/zsh-autosuggestions.zsh


### zsh-syntax-highlighting

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

source $ZPLUGDIR/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

### z

_Z_DATA=$HOME/.cache/zsh/z

source $ZPLUGDIR/z/z.sh

### zsh-autoswitch-virtualenv

if [ -x "$(command -v virtualenvwrapper.sh)" ]
then
    source =virtualenvwrapper.sh

    source $ZPLUGDIR/zsh-autoswitch-virtualenv/autoswitch_virtualenv.plugin.zsh
fi

### zsh-nix-shell
#
source $ZPLUGDIR/zsh-nix-shell/nix-shell.plugin.zsh
