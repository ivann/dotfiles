# Dotfiles

My dotfiles managed with make and stow.

## Installation

Install only vim and zsh dotfiles:

```
make base
```

Install everything:

```
make
```
